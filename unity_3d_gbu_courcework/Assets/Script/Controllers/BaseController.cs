﻿namespace HumanHunterGame
{
    /// <summary>
    /// Базовый класс для всех контроллеров
    /// </summary>
    public abstract class BaseController
    {
        /// <summary>
        /// Признак активности контроллера
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Включение контроллера
        /// </summary>
        public virtual void On()
        {
            On(null);
        }

        public virtual void On(BaseObjectScene obj = null)
        {
            IsActive = true;
        }

        public virtual void Off()
        {
            IsActive = false;
        }

        public void Switch()
        {
            if (IsActive)
            {
                Off();
            }
            else
            {
                On();
            }
        }

        /// <summary>
        /// Метод вызываемый при обновлении сцены
        /// </summary>
        public abstract void MyUpdate();
    }
}