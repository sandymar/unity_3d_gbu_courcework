﻿using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Контроллер игрока
    /// </summary>
	public class PlayerController : BaseController
	{
		private IMotor _unit;
        private bool _playerRun;
        private Vector2 _moveInputData;
        private bool _itJump;
        private float _inputXAxis;
        private float _inputYAxis;
        private float _playerSpeed;
        private Animator _animator;

        public PlayerController(IMotor player)
        {
            _unit        = player;
            _playerSpeed = Constants.HUMAN_WALK_SPEED;
            _animator    = Main.Instance.Player.GetComponent<Animator>();
        }

        #region Properties
        public Vector2 MoveInputData { set => _moveInputData = value; }
        public bool ItJump { set => _itJump = value; }
        public float InputXAxis { set => _inputXAxis = value; }
        public float InputYAxis { set => _inputYAxis = value; }
        public bool PlayerRun
        {
            set
            {
                _playerRun = value;
                if (_playerRun) _playerSpeed = Constants.HUMAN_RUN_SPEED;
                else _playerSpeed = Constants.HUMAN_WALK_SPEED;
            }

        }
        #endregion

        #region PublicMethods
        public override void MyUpdate()
		{
           _unit.Move(_playerSpeed, _moveInputData, _itJump, _inputXAxis, _inputYAxis);
            Animate();
        }
        #endregion

        /// <summary>
        /// Общий метод анимации движени игрока
        /// </summary>
        private void Animate()
        {
            if (_moveInputData.x != 0 || _moveInputData.y != 0)
            {
                if (_playerSpeed > Constants.HUMAN_WALK_SPEED)
                {
                    _animator.SetBool("itWalk", true);
                    _animator.SetBool("itRun", true);
                }
                else
                {
                    _animator.SetBool("itWalk", true);
                    _animator.SetBool("itRun", false);
                }
            }
            else
            {
                _animator.SetBool("itWalk", false);
                _animator.SetBool("itRun", false);
            }
        }
    }
}