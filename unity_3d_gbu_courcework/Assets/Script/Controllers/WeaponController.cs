﻿using HumanHunterGame.Weapons;
using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Контроллер работы с оружием
    /// </summary>
    public class WeaponController : BaseController
    {
        private bool _fire;
        private bool _holdWeapon;
        private Weapon _activeWeapon;
        private Weapon[] _allWeapons;
        private Animator _animator;

        public WeaponController()
        {
            _allWeapons = new Weapon[2];
            _allWeapons[0] = Main.Instance.Player.GetComponent<Sword>();
            _allWeapons[1] = Main.Instance.Player.GetComponent<Bow>();

            //выключаем всё оружие
            foreach (Weapon weapon in _allWeapons) weapon.Init(Main.Instance.Player.GetComponent<Animator>());

        }

        public bool Fire {set => _fire = value; }
        public bool HoldWeapon {set => _holdWeapon = value; }
        public Animator Animator { get => _animator; }
        internal Weapon[] AllWeapons { get => _allWeapons;}

        public override void MyUpdate()
        {
            if (!IsActive) return;

            if (_activeWeapon)
            {
                Main.Instance.UiInterface.WeaponUIText.ShowData(_activeWeapon.WeaponName, _activeWeapon.CountOfAmm, _activeWeapon.MaxOfAmm);

                //проверим ограничения по снарядам
                if (_activeWeapon.WeaponShooting)
                {
                    if (_activeWeapon.CountOfAmm == 0) return;
                }

                if (_fire)
                {
                    //убрали замах, перешли к удару
                    _holdWeapon = false;
                    _activeWeapon.Fire();
                    _fire = false;
                }
                if (_holdWeapon)
                {
                    _activeWeapon.Hold();
                }
            }
            else
            {
                Main.Instance.UiInterface.WeaponUIText.ShowData("", 0, 0);
            }
            
        }

        /// <summary>
        /// Меняет оружие на выбранное по цифре
        /// НЕ индекс! InputController знать не должен, как идет дальнейшая обработка
        /// </summary>
        /// <param name="weaponNum">Нажатая цифра</param>
        public void ChangeWeaponByNum(int weaponNum)
        {
            _fire = false;
            int chI = weaponNum - 1;
            //сначала нужно отыграть анимацию "выключения" оружия
            if (_activeWeapon) _activeWeapon.Change(false);// ActiveDeactiveWeapon(_activeWeapon, false);
            //теперь отыграть анимацию "включения"
            AllWeapons[chI].Change(true);// ActiveDeactiveWeapon(_allWeapons[chI], true);
            _activeWeapon = AllWeapons[chI];
        }

        /// <summary>
        /// вывывается, когда начинается удерживание оружия в замахе
        /// </summary>
        public void StartHold()
        {
            if (_activeWeapon)
            {
                _activeWeapon.StartHold();
            }
        }

        /// <summary>
        /// Вызывается, когда оружие выходит из замаха (удар или отмета действия)
        /// </summary>
        public void StopHold()
        {
            if (_activeWeapon)
            {
                _activeWeapon.StopHold();
            }
        }

        //TODO: Тут должно быть добавление боеприпасов
       
    }
}
