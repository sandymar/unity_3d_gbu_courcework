﻿using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Контроллер обработки данных от игрока
    /// </summary>
	public class InputController : BaseController
	{        
        private KeyCode _runKey   = KeyCode.LeftShift;
        private KeyCode _jumpKey = KeyCode.Space;
        private KeyCode _weapon1Key = KeyCode.Alpha1;
        private KeyCode _weapon2Key = KeyCode.Alpha2;
        private KeyCode _fireKey = KeyCode.Mouse0;
        private KeyCode _pickUpKey = KeyCode.E;

        public override void MyUpdate()
        {
            if (!IsActive) return;

            //Бегаем только с зажатым шифтом и только не во время нанесения удара
            if (Input.GetKeyDown(_runKey) && !Input.GetKey(_fireKey))
            {
                Main.Instance.PlayerController.PlayerRun = true;
            }
            else if (Input.GetKeyUp(_runKey))
            {
                Main.Instance.PlayerController.PlayerRun = false;
            }

            //смена оружия
            if (Input.GetKeyDown(_weapon1Key))
            {
                Main.Instance.WeaponController.ChangeWeaponByNum(1);
            }
            else if (Input.GetKeyDown(_weapon2Key))
            {
                Main.Instance.WeaponController.ChangeWeaponByNum(2);
            }
            else if (Input.GetKeyUp(_fireKey))
            {
                Main.Instance.WeaponController.Fire = true;
                Main.Instance.WeaponController.StopHold();
            }
            else if (Input.GetKeyDown(_fireKey))
            {
                Main.Instance.WeaponController.HoldWeapon = true;
                Main.Instance.WeaponController.StartHold();
            }
            else if (Input.GetKeyDown(_pickUpKey))
            {
                Main.Instance.SelectController.PickUpThing();
            }

                //отслеживаем движения
            Main.Instance.PlayerController.ItJump     = Input.GetKeyDown(_jumpKey);
            Main.Instance.PlayerController.InputXAxis = Input.GetAxis("Mouse X");
            Main.Instance.PlayerController.InputYAxis = Input.GetAxis("Mouse Y");
            Main.Instance.PlayerController.MoveInputData = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
    }
}