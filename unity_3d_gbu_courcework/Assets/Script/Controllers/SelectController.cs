﻿using HumanHunterGame.Ammunitions;
using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Класс предназначен для работы с "выделенными" объектами
    /// </summary>
    public class SelectController : BaseController
    {
        private Camera _mainCam;
        private Vector3 _cameraCenter;
        private Ray _ray;
        private RaycastHit _hit;
        private Transform _lastTransform;
        private BaseObjectScene _selectedT;

        public SelectController()
        {
            _mainCam = Camera.main;
            _cameraCenter = new Vector3(_mainCam.scaledPixelWidth / 2, _mainCam.scaledPixelHeight / 2, _mainCam.nearClipPlane);
        }

        public override void MyUpdate()
        {
            _cameraCenter.x = _mainCam.scaledPixelWidth / 2;
            _cameraCenter.y = _mainCam.scaledPixelHeight / 2;
            _ray = _mainCam.ScreenPointToRay(_cameraCenter);

            Debug.DrawRay(_ray.origin, _ray.direction * 100f, Color.blue);

            if (Physics.Raycast(_ray, out _hit, Constants.SELECT_MAX_DISTANCE_TO_DETECT, Constants.SELECT_LAYER_FOR_SELECT))
            {
                ISelectable tempObj = _hit.transform.gameObject.GetComponent<ISelectable>();
                if (tempObj != null)
                {   //простым элвисом тут неудобно
                    //отдаем интерфейсу инструкцию на отображение надписи
                    Main.Instance.UiInterface.SelectedUIText.ShowData(tempObj.NameOfObject);
                    if (_lastTransform != null && _lastTransform != _hit.transform) _lastTransform.gameObject.GetComponentInChildren<Outline_n>().enabled = false;
                    else _hit.transform.gameObject.GetComponentInChildren<Outline_n>().enabled = true;
                    _lastTransform = _hit.transform;
                    _selectedT = _hit.transform.gameObject.GetComponent<BaseObjectScene>();
                }
            }
            else if (_lastTransform != null)
            {
                Main.Instance.UiInterface.SelectedUIText.ShowData("");
                _lastTransform.gameObject.GetComponentInChildren<Outline_n>().enabled = false;
                _lastTransform = null;
            }
        }

        /// <summary>
        /// Метод вызывается при подборе предмета
        /// </summary>
        public void PickUpThing()
        {
            if (_lastTransform?.gameObject.GetComponent<Arrow>())
            {
                Main.Instance.WeaponController.AllWeapons[1].AddAmmo(1);
                _selectedT.DestroyMe();
            }

            //TODO: Аптеки и прочие хорошие вещи
        }
    }
}
