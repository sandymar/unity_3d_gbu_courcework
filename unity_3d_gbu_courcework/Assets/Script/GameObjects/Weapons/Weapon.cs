﻿using UnityEngine;
using HumanHunterGame.Ammunitions;

namespace HumanHunterGame.Weapons
{
    /// <summary>
    /// Базовый класс для оружия
    /// </summary>
    abstract class Weapon:BaseObjectScene
    {
        ///потребуется в перспективе, чторбы отделять "стреляющее" от "рубящего"
        [SerializeField] protected bool _weaponUseAmmunition;
        [SerializeField] private bool _weaponShooting;
        [SerializeField] protected bool _weaponUseHold;

        /// <summary>
        /// Тип используемых боеприпасов
        /// </summary>
        [SerializeField] protected Ammunition _ammunition;
        [SerializeField] protected Transform _barrel;
        /// <summary>
        /// Максимум переносимых с собой боеприпасов
        /// </summary>
        [SerializeField] private int _maxOfAmm;
        /// <summary>
        /// Текущее количество боепрпасов 
        /// </summary>
        [SerializeField] private int _countOfAmm;
        /// <summary>
        /// Интервал между ударами
        /// </summary>
        [SerializeField] protected float _reloadTime;
        /// <summary>
        /// Нименвоание оружия
        /// </summary>
        [SerializeField] private string _weaponName;
        [SerializeField] protected float _force;
        /// <summary>
        /// Данные по анимациям
        /// </summary>
        protected WeaponAnimData _weaponAnimData;
        /// <summary>
        /// Оружие готово к следующему использованию
        /// </summary>
        protected bool _isReady;
        /// <summary>
        /// Храним контроллер анимации
        /// </summary>
        protected Animator _animator;


        internal WeaponAnimData WeaponAnimData { get => _weaponAnimData;}
        public bool WeaponShooting { get => _weaponShooting; }
        public int MaxOfAmm { get => _maxOfAmm;}
        public int CountOfAmm { get => _countOfAmm;}
        public string WeaponName { get => _weaponName; protected set => _weaponName = value; }

        /// <summary>
        /// Инициализация оружия при запуске игры
        /// </summary>
        /// <param name="animator"></param>
        public virtual void Init(Animator animator)
        {
            _animator = animator;
            ActiveDeactiveWeapon(this, false);
        }

        /// <summary>
        /// Использовать оружие
        /// </summary>
        public virtual void Fire()
        {
            PlayOnFire();
            //проверим ограничения по снарядам
            if (_weaponShooting)
            {
                _countOfAmm--;
            }
        }

        /// <summary>
        /// Оружие готово к выстрелу
        /// </summary>
        public void IsReady()
        {
            _isReady = true;
            Debug.Log("it ready");
        }
        
        /// <summary>
        /// Замахнуться оружием
        /// </summary>
        public virtual void Hold()
        {
            //отрисовываем анимацию замаха оружием
            if (_weaponUseHold)
            {
                Debug.Log("it hold");
                PlayOnHold();
            }
        }
        
        /// <summary>
        /// Начато удержания оружия в замахе
        /// </summary>
        public virtual void StartHold()
        {
            _isReady = false;
            if (_countOfAmm != 0)
            {
                MyInvoke(IsReady, _reloadTime);
                MyInvokeRepeating(ShowLoadAnim, 0, _reloadTime / 10);
            }
        }

        /// <summary>
        /// Прекращено удержание оружия в замахе
        /// </summary>
        public virtual void StopHold()
        {
            if (_countOfAmm != 0)
            {
                CancelInvoke("IsReady");
                CancelInvoke("ShowLoadAnim");
                Main.Instance.UiInterface.HoldBowUI.ShowData(0);
            }
        }

        /// <summary>
        /// Показать анимацию подготовки к выстрелу
        /// </summary>
        private void ShowLoadAnim()
        {
            Main.Instance.UiInterface.HoldBowUI.NextTick();
        }

        /// <summary>
        /// Изменение оружия
        /// </summary>
        public virtual void Change(bool turnOn)
        {
            //отрисовываем анимацию замаха оружием
            _isReady = false;
            ActiveDeactiveWeapon(this, turnOn);
        }

        /// <summary>
        /// Добавить снаряды оружию
        /// </summary>
        /// <param name="count"></param>
        public void AddAmmo(int count)
        {
            _countOfAmm += count;
        }


        #region Animation
        /// <summary>
        /// Отыгрывает заданную оружию анимацию 
        /// </summary>
        /// <param name="weapon">Оружие</param>
        /// <param name="turnOn">Должно стать активным (true), неактивным (false)</param>
        private void ActiveDeactiveWeapon(Weapon weapon, bool turnOn)
        {
            foreach (GameObject go in weapon.WeaponAnimData.ActiveGO)
            {
                go.SetActive(turnOn);
            }

            foreach (GameObject go in weapon.WeaponAnimData.DeactiveGO)
            {
                go.SetActive(!turnOn);
            }
            if (turnOn) _animator.Play(weapon.WeaponAnimData.ActivationAnimName);
            else _animator.Play(weapon.WeaponAnimData.DectivationAnimName);
        }

        /// <summary>
        /// Проиграть анимацию при ударе
        /// </summary>
        private void PlayOnFire()
        {
            //убираем удержание оружия
            _animator.SetBool("itHold", false);
            _animator.Play(WeaponAnimData.FireAnimation);
        }

        /// <summary>
        /// Проиграть анимацию при удержании оружия
        /// </summary>
        private void PlayOnHold()
        {
            _animator.SetBool("itHold", true);
        }
        #endregion
    }
}
