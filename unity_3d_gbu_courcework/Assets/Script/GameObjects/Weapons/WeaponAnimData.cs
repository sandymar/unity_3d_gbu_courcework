﻿using UnityEngine;

namespace HumanHunterGame.Weapons
{
    /// <summary>
    /// Тут храним данные по именам проигрываемых анимаций и объектам
    /// </summary>
    struct WeaponAnimData
    {
        /// <summary>
        /// Массив игровых объектов, которые нужно будет активировать при использовании оружия
        /// </summary>
        public GameObject[] ActiveGO;
        /// <summary>
        /// Массив игровых объектов, которые нужно будет активировать при прекращении использования оружия
        /// </summary>
        public GameObject[] DeactiveGO;
        /// <summary>
        /// Наименование анимации для активации оружия
        /// </summary>
        public string ActivationAnimName;
        /// <summary>
        /// Наименование анимации для деактивации оружия
        /// </summary>
        public string DectivationAnimName;
        /// <summary>
        /// Наименование анимации для удара оружием
        /// </summary>
        public string FireAnimation;
        /// <summary>
        /// Наименование анимации для замаха оружием
        /// </summary>
        //public string HoldAnimation;
    }
}
