﻿using HumanHunterGame.Ammunitions;
using UnityEngine;

namespace HumanHunterGame.Weapons
{
    /// <summary>
    /// Лук
    /// </summary>
    sealed class Bow : Weapon
    {
        private float _fullCameraRange;
        private MeshRenderer _arrowMesh;
        protected override void Awake()
        {
            base.Awake();
            _weaponAnimData = new WeaponAnimData();
            _weaponAnimData.ActiveGO = new UnityEngine.GameObject[3];
            _weaponAnimData.ActiveGO[0] = UnityEngine.GameObject.Find("Hunter_Arrow");
            _weaponAnimData.ActiveGO[1] = UnityEngine.GameObject.Find("Hunter_Bow001");
            _weaponAnimData.ActiveGO[2] = UnityEngine.GameObject.Find("Adam_Bow_Adv_Hand_Main_Bone003");
            _weaponAnimData.DeactiveGO = new UnityEngine.GameObject[1];
            _weaponAnimData.DeactiveGO[0] = UnityEngine.GameObject.Find("Hunter_Bow");
            _weaponAnimData.ActivationAnimName = "TookBowOut";
            _weaponAnimData.DectivationAnimName = "";
            _weaponAnimData.FireAnimation = "AttackBow";
            _weaponUseHold = true;
            _fullCameraRange = Mathf.Abs(Constants.CAMERA_MAXIMUM_X) + Mathf.Abs(Constants.CAMERA_MINIMUM_X);
            WeaponName = "Long bow";

            _arrowMesh = GameObject.Find("Hunter_Arrow").GetComponent<MeshRenderer>();
        }

        /// <summary>
        /// Удержание лука с натянутой тетивой
        /// </summary>
        public override void Hold()
        {
            _arrowMesh.enabled = true;
            base.Hold();
            float actX = Camera.main.transform.rotation.eulerAngles.x;
            float xVal = 0;

            if (actX > 180)
                xVal = Mathf.Abs(Constants.CAMERA_MINIMUM_X)-(360 - actX);
            else
                xVal = Mathf.Abs(Constants.CAMERA_MINIMUM_X) + actX;
            float animT = 1 / _fullCameraRange * xVal;

            //теперь корректируем позицию по наклону камеры. 
            _animator.SetFloat("HoldPosition", animT);
        }

        /// <summary>
        /// Выстрел из лука
        /// </summary>
        public override void Fire()
        {
            if (!_isReady)
            {
                _animator.SetBool("itHold", false);
                return;
            }
            _arrowMesh.enabled = false;
            base.Fire();
            Vector3 _cameraCenter = new Vector3(Camera.main.scaledPixelWidth / 2, Camera.main.scaledPixelHeight / 2, Camera.main.nearClipPlane);
            Vector3 end = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 2f));
            Vector3 dir = Camera.main.ScreenPointToRay(_cameraCenter).direction;
            Debug.DrawRay(end, dir.normalized * _force, Color.yellow);            
            Quaternion rot = Quaternion.LookRotation(dir);
            Ammunition arrowGO = Instantiate(_ammunition, end, rot);
            arrowGO.AddForce(arrowGO.transform.forward  * _force);
        }
    }
}
