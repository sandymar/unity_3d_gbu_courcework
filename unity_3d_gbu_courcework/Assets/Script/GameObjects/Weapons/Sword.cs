﻿namespace HumanHunterGame.Weapons
{
    /// <summary>
    /// Меч
    /// </summary>
    sealed class Sword : Weapon
    {
        protected override void Awake()
        {
            base.Awake();
            _weaponAnimData = new WeaponAnimData();
            _weaponAnimData.ActiveGO = new UnityEngine.GameObject[1];
            _weaponAnimData.ActiveGO[0] = UnityEngine.GameObject.Find("Hunter_Sword");
            _weaponAnimData.DeactiveGO = new UnityEngine.GameObject[1];
            _weaponAnimData.DeactiveGO[0] = UnityEngine.GameObject.Find("Hunter_Sword001");
            _weaponAnimData.ActivationAnimName = "TookSwordOut";
            _weaponAnimData.DectivationAnimName = "";
            _weaponAnimData.FireAnimation = "AttackSword";
            _weaponUseHold = false;

            WeaponName = "Light sword";
        }

    }
}
