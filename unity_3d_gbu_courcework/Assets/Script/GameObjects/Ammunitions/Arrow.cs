﻿using UnityEngine;

namespace HumanHunterGame.Ammunitions
{
    /// <summary>
    /// Стрела для лука
    /// </summary>
    sealed class Arrow : Ammunition, ISelectable
    {
        private string _nameOfObject = "Arrow";

        public string NameOfObject { get => _nameOfObject; set => _nameOfObject = value; }

        private void OnCollisionEnter(UnityEngine.Collision collision)
        {
            //отключаем физику стрелы
            DisableRigidBody();
            //TODO: Рассчитывать глубину проникновения стрелы (или, наоборот, отскакивание) в зависимости от скорости и материала цели
            //двигаем стрелу немного вперед.
            //переключаю коллайдеры в режим триггера
            SetTriggerForAllCollider(true);
            if (!collision.collider.name.Equals("Terrain"))
                transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y + 0.1f, transform.position.z + 0.1f);
            
            //Делаем дочерним объектом цели (чтобы стрела застревала в меесте попадания
            transform.parent = collision.gameObject.transform;

            //TODO: Предумотреть коллайдеры по "частям тела" в противниках, в которых будет засревать стрела.
            //TODO: Добавить расчет урона в зависимости от точки попадания стрелы
        }
    }
}
