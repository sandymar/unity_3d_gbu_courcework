﻿using UnityEngine;

namespace HumanHunterGame.Ammunitions
{
    /// <summary>
    /// Базовый класс для боеприпасов
    /// </summary>
    class Ammunition:BaseObjectScene
    {
        /// <summary>
        /// "Выстрел" боеприпаса с позицией из центра экрана
        /// </summary>
        /// <param name="force">Усилие выстрела</param>
        public void AddForce(float force)
        {
            Vector3 _cameraCenter = new Vector3(Camera.main.scaledPixelWidth / 2, Camera.main.scaledPixelHeight / 2, Camera.main.nearClipPlane);
            _cameraCenter.x = Camera.main.scaledPixelWidth / 2;
            _cameraCenter.y = Camera.main.scaledPixelHeight / 2;
            AddForce(Camera.main.ScreenPointToRay(_cameraCenter).direction*force);
        }

        /// <summary>
        /// "Выстрел" боеприпаса по указанному вектору
        /// </summary>
        /// <param name="dir"></param>
        public void AddForce(Vector3 dir)
        {
            if (!Rigidbody) return;
            Rigidbody.AddForce(dir);
        }
    }
}
