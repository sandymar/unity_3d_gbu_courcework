﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Основной игровой класс, работает в режиме синглтона, обеспечивает функциональность игровых контроллеров
    /// </summary>
    public sealed class Main:MonoBehaviour
    {
        public static Main Instance;
        [HideInInspector]
        public Transform Player;
        public PlayerController PlayerController;
        public InputController InputController;
        public SelectController SelectController;
        public WeaponController WeaponController;
        public UiInterface UiInterface;

        //для обработки собственного апдейта
        private BaseController[] _controllers;
        [SerializeField]
        private Transform _playerPrefab;

        //для теста
        public Transform CameraBonePos;

        private void Awake()
        {
            Instance = this;
            SpawnAll();

            PlayerController = new PlayerController(new UnitMotor(Player));
            InputController = new InputController();
            InputController.On();
            SelectController = new SelectController();
            SelectController.On();
            WeaponController = new WeaponController();
            WeaponController.On();

            _controllers = new BaseController[4];
            _controllers[0] = InputController;
            _controllers[1] = PlayerController;
            _controllers[2] = SelectController;
            _controllers[3] = WeaponController;

            UiInterface = new UiInterface();
    }

        /// <summary>
        /// Обновляем состояния наших контроллеров
        /// </summary>
        private void Update()
        {
            //вбиваю странный костыль - юнити деактивирует моего игрока по неясным причинам
            if (!Player.gameObject.active) Player.gameObject.SetActive(true);
            //\\
            foreach (BaseController controller in _controllers)
            {
                controller.MyUpdate();
            }

        }


        #region Private
        /// <summary>
        /// Спаун игрока на одной из существующих на карте точек спауна
        /// Временно отключено
        /// </summary>
        private void SpawnAll()
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(Constants.TAG_PLAYER_SPAWN_POSITION);
            int sp = UnityEngine.Random.Range(0, gameObjects.Length);
            //Вернуть завершения левелдизайна
            Player = Instantiate(_playerPrefab, gameObjects[sp].transform.position, Quaternion.identity);
            //Player = _playerPrefab;
        }
        #endregion

        #region Public

        /// <summary>
        /// Обертка для запуска корутины
        /// </summary>
        /// <param name="routine"></param>
        public void DoStartCoroutine(IEnumerator routine)
        {
            StartCoroutine(routine);
        }

        /// <summary>
        /// Обертка для остановки корутины
        /// </summary>
        /// <param name="routine"></param>
        public void DoStopCoroutine(IEnumerator routine)
        {
            StopCoroutine(routine);
        }
        #endregion
    }
}
