﻿using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Класс для управления UI
    /// </summary>
    public class UiInterface
    {
        private SelectedUIText _selectedUIText;
        private WeaponUIText _weaponUIText;
        private HoldBowUI _holdBowUI;

        internal SelectedUIText SelectedUIText
        {
            get
            {
                if (!_selectedUIText)
                {
                    _selectedUIText = MonoBehaviour.FindObjectOfType<SelectedUIText>();
                }
                return _selectedUIText;
            }
        }

        internal WeaponUIText WeaponUIText
        {
            get
            {
                if (!_weaponUIText)
                {
                    _weaponUIText = MonoBehaviour.FindObjectOfType<WeaponUIText>();
                }
                return _weaponUIText;
            }
        }

        internal HoldBowUI HoldBowUI
        {
            get
            {
                if (!_holdBowUI)
                {
                    _holdBowUI = MonoBehaviour.FindObjectOfType<HoldBowUI>();
                }
                return _holdBowUI;
            }
        }
    }
}