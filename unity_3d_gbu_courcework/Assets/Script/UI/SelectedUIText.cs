﻿using UnityEngine;
using UnityEngine.UI;

namespace HumanHunterGame
{
    /// <summary>
    /// Класс для отображения надписи выбранного предмета
    /// </summary>
    class SelectedUIText:MonoBehaviour
    {
        private Text _text;

        private void Start()
        {
            _text = GetComponent<Text>();
            _text.color = Constants.UI_TEXT_COLOR;
        }

        public void ShowData(string selectedText)
        {
            _text.text = $"{selectedText}";
        }

        public void SetActive(bool value)
        {
            _text.gameObject.SetActive(value);
        }
    }
}
