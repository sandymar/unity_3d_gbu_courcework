﻿using UnityEngine;
using UnityEngine.UI;

namespace HumanHunterGame
{
    /// <summary>
    /// Класс для отображдения кольца "подготовки оружия"
    /// </summary>
    class HoldBowUI:MonoBehaviour
    {
        private Image _image;
        private void Start()
        {
            _image = GetComponent<Image>();
            _image.fillAmount = 0;
        }

        public void ShowData(float _fillData)
        {
            _image.fillAmount = _fillData;
        }

        public void NextTick()
        {
            _image.fillAmount = _image.fillAmount + 0.1f;
        }

        public void SetActive(bool val)
        {
            _image.gameObject.SetActive(val);
        }

    }
}
