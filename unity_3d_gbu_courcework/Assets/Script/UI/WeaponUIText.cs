﻿using UnityEngine;
using UnityEngine.UI;

namespace HumanHunterGame
{
    /// <summary>
    /// Класс для отображения информации по оружию
    /// </summary>
    class WeaponUIText:MonoBehaviour
    {
        private Text _text;

        private void Start()
        {
            _text = GetComponent<Text>();
            _text.color = Constants.UI_TEXT_COLOR;
        }

        public void ShowData(string weaponName,float currVal, float maxVal)
        {
            if (maxVal == 0f) _text.text = $"{weaponName}";
            else _text.text = $"{weaponName} {currVal}/{maxVal}";
        }

        public void SetActive(bool value)
        {
            _text.gameObject.SetActive(value);
        }
    }
}
