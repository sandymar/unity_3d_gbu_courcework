﻿namespace HumanHunterGame
{
    /// <summary>
    /// Интерфейс для выделяемых объектов
    /// </summary>
    interface ISelectable
    {
        /// <summary>
        /// Имя выделяемого объекта
        /// </summary>
        string NameOfObject { get; set; }
    }
}
