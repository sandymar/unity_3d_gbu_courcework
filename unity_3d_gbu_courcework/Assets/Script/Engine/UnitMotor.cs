﻿using UnityEngine;

namespace HumanHunterGame
{
   /// <summary>
   /// Основной движок игрока
   /// </summary>
   public class UnitMotor : IMotor
    {
        private readonly float MAX_X_ANGLE = 45;

        private Transform _instance;

        private float _speedMove = 10;
        private float _jumpPower = 10;
        private float _gravityForce;
        private Vector2 _input;
        private Vector3 _moveVector;
        private CharacterController _characterController;
        private Transform _head;

        private float XSensitivity = 2f;
        private float YSensitivity = 2f;
        private bool ClampVerticalRotation = true;
        private float MinimumX = Constants.CAMERA_MINIMUM_X;
        private float MaximumX = Constants.CAMERA_MAXIMUM_X;
        private bool Smooth;
        private float SmoothTime = 5f;
        private Quaternion _characterTargetRot;
        private Quaternion _cameraTargetRot;

        public UnitMotor(Transform instance)
        {
            _instance = instance;
            _characterController = _instance.GetComponent<CharacterController>();
            _head = Camera.main.transform;

            _characterTargetRot = _instance.localRotation;
            _cameraTargetRot = _head.localRotation;

            //_characterTargetRot = Main.Instance.CameraBonePos.localRotation;
            //_cameraTargetRot = Main.Instance.CameraBonePos.localRotation;
        }

        /// <summary>
        /// Каскад процедур движения с перегрузками
        /// В идеале должно быть никакого контроля ввода в коде движка
        /// В случаях без параметров Input'ы дергаются, но сие скорее для примера
        /// </summary>
        #region MovePublicCascade
        public void Move() => Move(_speedMove);

        public void Move(float moveSpeed)=>
            Move(moveSpeed, new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));        

        public void Move(float moveSpeed, Vector2 moveInputData)=>
            Move(moveSpeed, moveInputData, Input.GetKeyDown(KeyCode.Space));

        public void Move(float moveSpeed, Vector2 moveInputData, bool itJump)=>
            Move(moveSpeed, moveInputData, itJump, Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        /// <summary>
        /// Обработчик движения игрока
        /// </summary>
        /// <param name="moveSpeed">Скорость движения</param>
        /// <param name="moveInputData">направление движения</param>
        /// <param name="itJump">Признак прыжка</param>
        /// <param name="inputXAxis">Данные мыши по X-оси</param>
        /// <param name="inputYAxis">Данные мыши по Y-оси</param>
        public void Move(float moveSpeed, Vector2 moveInputData, bool itJump, float inputXAxis, float inputYAxis)
        {
            _speedMove = moveSpeed;
            CharacterMove(moveInputData);
            GamingGravity(itJump);
            LookRotation(_instance, _head,inputXAxis,inputYAxis);
        }
        #endregion

        /// <summary>
        /// Движение персонажа
        /// </summary>
        /// <param name="inputData"></param>
        private void CharacterMove(Vector2 inputData)
        {
            if (_characterController.isGrounded)
            {
                _input = inputData;
                Vector3 desiredMove = _instance.forward * _input.y + _instance.right * _input.x;
                _moveVector.x = desiredMove.x * _speedMove;
                _moveVector.z = desiredMove.z * _speedMove;
            }

            _moveVector.y = _gravityForce;
            _characterController.Move(_moveVector * Time.deltaTime);
        }
        
        /// <summary>
        /// Игровая гравитация
        /// </summary>
        /// <param name="itJump"></param>
        private void GamingGravity(bool itJump)
        {
            if (!_characterController.isGrounded) _gravityForce -= 30 * Time.deltaTime;
            else _gravityForce = -1;
            if (itJump && _characterController.isGrounded) _gravityForce = _jumpPower;
        }

        /// <summary>
        /// Отслеживание камеры
        /// </summary>
        /// <param name="character">Позиция персонажа</param>
        /// <param name="camera">Ссылка на камеру</param>
        /// <param name="inputXAxis">Данные по X-оси мыши</param>
        /// <param name="inputYAxis">Данные по Y-оси мыши</param>
        private void LookRotation(Transform character, Transform camera, float inputXAxis, float inputYAxis)
        {
            float yRot = inputXAxis * XSensitivity;
            float xRot = inputYAxis * YSensitivity;

            _characterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
            _cameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

            if (ClampVerticalRotation)
                _cameraTargetRot = ClampRotationAroundXAxis(_cameraTargetRot);

            if (Smooth)
            {
                character.localRotation = Quaternion.Slerp(character.localRotation, _characterTargetRot,
                    SmoothTime * Time.deltaTime);
                camera.localRotation = Quaternion.Slerp(camera.localRotation, _cameraTargetRot,
                    SmoothTime * Time.deltaTime);
            }
            else
            {
                character.localRotation = _characterTargetRot;
                camera.localRotation = _cameraTargetRot;
            }
        }

        /// <summary>
        /// Обработчик поворота камеры
        /// </summary>
        /// <param name="q">Кватернион поворота</param>
        /// <returns></returns>
        private Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;
            

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }
    }

}