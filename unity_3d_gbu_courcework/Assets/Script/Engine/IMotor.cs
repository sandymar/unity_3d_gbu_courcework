﻿using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Интерфейс для движущегося объекта (игрока)
    /// </summary>
	public interface IMotor
	{
        /// <summary>
        /// Метод, выполняющий двжиение объекта
        /// </summary>
        void Move();
        /// <summary>
        /// Метод, выполняющий двжиение объекта
        /// </summary>
        /// <param name="moveSpeed">Скорость движения</param>
        void Move(float moveSpeed);
        /// <summary>
        /// Метод, выполняющий двжиение объекта
        /// </summary>
        /// <param name="moveSpeed">Скорость движения</param>
        /// <param name="moveInputData">Направление движенияч</param>
        void Move(float moveSpeed, Vector2 moveInputData);
        /// <summary>
        /// Метод, выполняющий двжиение объекта
        /// </summary>
        /// <param name="moveSpeed">Скорость движения</param>
        /// <param name="moveInputData">Направление движенияч</param>
        /// <param name="itJump">Признак того, что это прыжок</param>
        void Move(float moveSpeed, Vector2 moveInputData, bool itJump);
        /// <summary>
        /// Метод, выполняющий двжиение объекта
        /// </summary>
        /// <param name="moveSpeed">Скорость движения</param>
        /// <param name="moveInputData">Направление движенияч</param>
        /// <param name="itJump">Признак того, что это прыжок</param>
        /// <param name="inputXAxis">Положение мыши по оси X</param>
        /// <param name="inputYAxis">Положение мыши по оси Y</param>
        void Move(float moveSpeed, Vector2 moveInputData, bool itJump, float inputXAxis, float inputYAxis);
    }
}