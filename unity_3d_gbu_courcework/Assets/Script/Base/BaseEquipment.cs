﻿using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Базовый класс экипировки
    /// </summary>
    [RequireComponent(typeof(Outline_n))]
    class BaseEquipment : BaseObjectScene, ISelectable
    {
        private string _nameOfObject;
        private Outline_n _outline;
        //Каждый игровой объект, с которым будет хоть какое-то взаимодействие, должен быть подсвечен
        //указываем цвет подсветки по умолчанию
        private Color _outlineColor;

        #region Property

        /// <summary>
        /// Назначаем цвет подсветки при выделении
        /// </summary>
        protected Color OutlineColor
        {
            set
            {
                _outlineColor = value;
                OutlineColorChange(_outlineColor);
            }
        }

        /// <summary>
        /// Название игрового объекта для отображения
        /// </summary>
        public string NameOfObject { get => _nameOfObject; set => _nameOfObject = value; }
        #endregion

        #region UnityFunctions
        protected virtual void Start()
        {
            _outline = GetComponent<Outline_n>();
            _outline.enabled = false;
            OutlineColorChange(Constants.OUTLINE_COLOR);
            OutlineWidthChange(Constants.OUTLINE_WIDTH);
            _nameOfObject = gameObject.name;

            //всe предметы по умолчанию должны лежать на конкретном слое. Пусть с этим разбирается скрипт
            Layer = Constants.BASE_EQ_LAYER_MASK;
        }
        #endregion

        #region Private
        /// <summary>
        /// Изменение цвета обводки при выделении
        /// </summary>
        /// <param name="color"></param>
        private void OutlineColorChange(Color color)
        {
            _outline.OutlineColor = color;
        }
        /// <summary>
        /// Изменение тощины обводки при выделении
        /// </summary>
        /// <param name="width"></param>
        private void OutlineWidthChange(float width)
        {
            _outline.OutlineWidth = width;
        }
        #endregion
    }

}
