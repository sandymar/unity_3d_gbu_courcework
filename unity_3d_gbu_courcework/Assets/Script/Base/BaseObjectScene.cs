﻿using System;
using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Базовый класс для всех объектов 
    /// </summary>
    public abstract class BaseObjectScene : MonoBehaviour
    {
        private int _layer;
        private Color _color;
        private bool _isVisible;
        
        [HideInInspector] public Rigidbody Rigidbody;

        #region UnityFunction

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        #endregion

        #region Property

        /// <summary>
        /// Слой объекта
        /// </summary>
        public int Layer
        {
            get { return _layer; }

            set
            {
                _layer = value;
                AskLayer(transform, value);
            }
        }

        /// <summary>
        /// Цвет материала объекта
        /// </summary>
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                AskColor(transform, _color);
            }
        }

        /// <summary>
        /// Возвращает признак видимости объекта
        /// </summary>
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                var tempRenderer = GetComponent<Renderer>();
                if (tempRenderer)
                    tempRenderer.enabled = _isVisible;
                if (transform.childCount <= 0) return;
                foreach (Transform d in transform)
                {
                    tempRenderer = d.gameObject.GetComponent<Renderer>();
                    if (tempRenderer)
                        tempRenderer.enabled = _isVisible;
                }
            }
        }

        #endregion

        #region PrivateFunction
        /// <summary>
        /// Выставляет слой себе и всем вложенным объектам в независимости от уровня вложенности
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="lvl">Слой</param>
        private void AskLayer(Transform obj, int lvl)
        {
            obj.gameObject.layer = lvl;
            if (obj.childCount <= 0) return;
            foreach (Transform d in obj)
            {
                AskLayer(d, lvl);
            }
        }

        /// <summary>
        /// Рекурсивное изменение цвета объекта с вложенными
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="color"></param>
        private void AskColor(Transform obj, Color color)
        {
            foreach (var curMaterial in obj.GetComponent<Renderer>().materials)
            {
                curMaterial.color = color;
            }
            if (obj.childCount <= 0) return;
            foreach (Transform d in obj)
            {
                AskColor(d, color);
            }
        }
        #endregion

        /// <summary>
        /// Возвращает признак наличия физики у объекта
        /// </summary>
        /// <returns></returns>
        public bool IsRigitBody()
        {
            return Rigidbody;
        }

        /// <summary>
        /// Выключает физику у объекта и его детей
        /// </summary>
        public void DisableRigidBody()
        {
            if (!IsRigitBody()) return;

            Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
            foreach (var rb in rigidbodies)
            {
                rb.isKinematic = true;
            }
        }

        /// <summary>
        /// Переключение всех коллайдеров в режим триггера
        /// </summary>
        /// <param name="itTrigger">Устанавливаемый признак триггера</param>
        public void SetTriggerForAllCollider(bool itTrigger)
        {
            Collider[] colliders = GetComponentsInChildren<Collider>();
            foreach (Collider c in colliders) c.isTrigger = itTrigger;
        }

        /// <summary>
        /// Включает физику у объекта и его детей
        /// </summary>
        public void EnableRigidBody(float force)
        {
            EnableRigidBody();
            //Rigidbody.isKinematic = false;
            Rigidbody.AddForce(transform.forward * force);
        }

        /// <summary>
        /// Включает физику у объекта и его детей
        /// </summary>
        public void EnableRigidBody()
        {
            if (!IsRigitBody()) return;
            Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
            foreach (var rb in rigidbodies)
            {
                rb.isKinematic = false;
            }
        }

        /// <summary>
        /// Замораживает или размораживает физическую трансформацию объекта
        /// </summary>
        /// <param name="rigidbodyConstraints">Трансформацию которую нужно заморозить</param>
        public void ConstraintsRigidBody(RigidbodyConstraints rigidbodyConstraints)
        {
            Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
            foreach (var rb in rigidbodies)
            {
                rb.constraints = rigidbodyConstraints;
            }
        }
           
        /// <summary>
        /// Устанавливает признак активности объекта
        /// </summary>
        /// <param name="value"></param>
        public void SetActive(bool value)
        {
            IsVisible = value;

            var tempCollider = GetComponent<Collider>();
            if (tempCollider)
            {
                tempCollider.enabled = value;
            }
        }

        /// <summary>
        /// уничтожает объект на сцене. Предназначен для вызова из контроллеров
        /// </summary>
        public void DestroyMe()
        {
            Destroy(gameObject);
        }

        /// <summary>
        /// Обертка для Invoke метода
        /// </summary>
        /// <param name="method">Метод</param>
        /// <param name="time">Интервал вызова</param>
        protected void MyInvoke(Action method, float time)
        {
            Invoke(method.Method.Name, time);
        }

        /// <summary>
        /// Обертка для отмены Invoke-метода
        /// </summary>
        /// <param name="method">Отменяемый метод</param>
        protected void MyCancelInvoke(Action method)
        {
            CancelInvoke(method.Method.Name);
        }

        /// <summary>
        /// Обертка для повторяющегося метода
        /// </summary>
        /// <param name="method">Вызываемый метод</param>
        /// <param name="time">Интервал времени для запуска</param>
        /// <param name="repeatRate">Частота повтора</param>
        protected void MyInvokeRepeating(Action method, float time, float repeatRate)
        {
            InvokeRepeating(method.Method.Name, time, repeatRate);
        }

        /// <summary>
        /// Обработчик события при отключении объекта
        /// </summary>
        protected virtual void OnDisable()
        {
            CancelInvoke();
        }
    }
}