﻿using UnityEngine;

namespace HumanHunterGame
{
    /// <summary>
    /// Служебный класс для хранения констант и прочих магических чисел
    /// </summary>
    internal static class Constants
    {
        /// <summary>
        /// Цвет обводки предмета по умолчанию
        /// </summary>
        internal static readonly Color OUTLINE_COLOR = new Color32(0, 0, 255, 255);
        /// <summary>
        /// Толщина обводки предмпета по умолчанию
        /// </summary>
        internal static readonly float OUTLINE_WIDTH = 3;
        /// <summary>
        /// Дистанция до определения предмета
        /// </summary>
        internal static readonly float SELECT_MAX_DISTANCE_TO_DETECT = 10;
        /// <summary>
        /// Слой для "выбираемых" объектоы
        /// </summary>
        internal static readonly int SELECT_LAYER_FOR_SELECT = LayerMask.GetMask("Equipment");
        /// <summary>
        /// Слой для назначения по умолчанию всей создаваемой экипировке (может не совпадать с предыдущим)
        /// </summary>
        internal static readonly int BASE_EQ_LAYER_MASK = LayerMask.NameToLayer("Equipment");

        #region ReservedTags
        /// <summary>
        /// Тэги для точек спауна игрока
        /// </summary>
        internal static readonly string TAG_PLAYER_SPAWN_POSITION = "PlayerSpawnPosition";
        #endregion
        /// <summary>
        /// Код цвета пользовательсткого интерфейса
        /// </summary>
        internal static readonly Color UI_TEXT_COLOR = new Color32(240, 136, 105, 255);

        /// <summary>
        /// Определяем единые для всех человеческих существ скорость ходьбы и бега
        /// </summary>
        internal static readonly float HUMAN_WALK_SPEED = 3;
        internal static readonly float HUMAN_RUN_SPEED = 10;

        /// <summary>
        /// Максимальный и минимальный наклон камеры
        /// </summary>
        internal static readonly float CAMERA_MINIMUM_X = -40f;
        internal static readonly float CAMERA_MAXIMUM_X = 60f;
    }
}
